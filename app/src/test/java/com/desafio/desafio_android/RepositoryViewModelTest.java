package com.desafio.desafio_android;

import com.desafio.desafio_android.manager.GithubManager;
import com.desafio.desafio_android.model.Repository;
import com.desafio.desafio_android.model.RequestGithub;
import com.desafio.desafio_android.presenter.RepositoryPresenter;
import com.desafio.desafio_android.viewmodel.RepositoryViewModel;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Single;
import io.reactivex.subjects.PublishSubject;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.junit.MatcherAssert.assertThat;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.notNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class RepositoryViewModelTest {

    private RepositoryPresenter repositoryPresenter;
    private PublishSubject<Integer> publishSubject;
    private RepositoryViewModel repositoryViewModel;
    private GithubManager githubManager;

    @Before
    public void setup() {
        githubManager = mock(GithubManager.class);
        publishSubject = PublishSubject.create();
        repositoryPresenter = mock(RepositoryPresenter.class);
        repositoryViewModel = new RepositoryViewModel(publishSubject, githubManager, repositoryPresenter);
    }

    private List<Repository> mockRepoList() {
        List<Repository> repos = new ArrayList<>();
        Repository repo = new Repository();
        Repository repo2 = new Repository();
        repos.add(repo);
        repos.add(repo2);

        return repos;
    }

    @Test
    public void testLoadFromApiOk() throws Exception {

        RequestGithub githubRequest = mock(RequestGithub.class);

        when(githubRequest.getRepositories()).thenReturn(mockRepoList());

        when(githubManager.getGithubRepositories(anyInt())).thenReturn(Single.just(githubRequest));

        repositoryViewModel.startLoad(false);

        assertThat(repositoryViewModel.getItems().size(), equalTo(3));
    }

    @Test
    public void testLoadFromApiFail() throws Exception {
        RequestGithub githubRequest = mock(RequestGithub.class);

        Throwable th = new Throwable("Fail");

        when(githubManager.getGithubRepositories(anyInt())).thenReturn(Single.error(th));

        repositoryViewModel.startLoad(false);

        verify(repositoryPresenter).showError(th);
    }
}