package com.desafio.desafio_android.client;

import com.desafio.desafio_android.model.PullRequest;
import com.desafio.desafio_android.model.RequestGithub;

import java.util.List;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface GithubClient {

    @GET("search/repositories")
    Single<RequestGithub> getRepositories(@Query("q") String language, @Query("sort") String sort, @Query("page") int page);

    @GET("repos/{creator}/{repository}/pulls")
    Single<List<PullRequest>> getPullRequets(@Path("creator") String creator, @Path("repository") String repository);
}
