package com.desafio.desafio_android.application;

import android.app.Application;

import com.desafio.desafio_android.di.ApplicationComponent;
import com.desafio.desafio_android.di.ApplicationModule;
import com.desafio.desafio_android.di.DaggerApplicationComponent;

public class GithubApplication extends Application {
    private ApplicationComponent applicationComponent;

    @SuppressWarnings("deprecation")
    @Override
    public void onCreate() {
        super.onCreate();

        applicationComponent = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule("https://api.github.com/"))
                .build();
    }

    public ApplicationComponent getApplicationComponent() {
        return applicationComponent;
    }
}
