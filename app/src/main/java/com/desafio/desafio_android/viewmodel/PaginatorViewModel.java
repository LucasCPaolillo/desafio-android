package com.desafio.desafio_android.viewmodel;

public class PaginatorViewModel implements CommonPaginatorInterfaceRow {
    String page;

    public PaginatorViewModel(String page) {
        this.page = page;
    }

    public String getPage() {
        return page;
    }
}
