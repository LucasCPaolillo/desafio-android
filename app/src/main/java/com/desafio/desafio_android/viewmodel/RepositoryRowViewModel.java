package com.desafio.desafio_android.viewmodel;

import com.desafio.desafio_android.model.Repository;

public class RepositoryRowViewModel implements CommonPaginatorInterfaceRow{
    private Repository repository;
    private RepositoryViewModel repositoryViewModel;

    public RepositoryRowViewModel(RepositoryViewModel repositoryViewModel, Repository repository) {
        this.repositoryViewModel = repositoryViewModel;
        this.repository = repository;
    }

    public Repository getRepository() {
        return repository;
    }

    public void click() {
        repositoryViewModel.click(repository);
    }
}
