package com.desafio.desafio_android.viewmodel;

import android.databinding.ObservableArrayList;
import android.databinding.ObservableField;

import com.desafio.desafio_android.BR;
import com.desafio.desafio_android.R;
import com.desafio.desafio_android.manager.GithubManager;
import com.desafio.desafio_android.model.Repository;
import com.desafio.desafio_android.model.RequestGithub;
import com.desafio.desafio_android.presenter.RepositoryPresenter;

import java.io.Serializable;

import io.reactivex.subjects.PublishSubject;
import me.tatarka.bindingcollectionadapter.BaseItemViewSelector;
import me.tatarka.bindingcollectionadapter.ItemView;
import me.tatarka.bindingcollectionadapter.ItemViewSelector;

public class RepositoryViewModel extends CommonViewModel implements Serializable {

    private ObservableArrayList<CommonPaginatorInterfaceRow> items;
    private PublishSubject<Integer> onPageChange;
    private RepositoryPresenter repositoryPresenter;
    private ObservableField<Boolean> progressBarVisibility;
    private int page = 1;


    public RepositoryViewModel(PublishSubject<Integer> onPageChange, GithubManager githubManager, RepositoryPresenter repositoryPresenter) {
        super(githubManager);
        this.progressBarVisibility = new ObservableField<>(false);
        items = new ObservableArrayList<>();
        this.onPageChange = onPageChange;
        this.onPageChange.subscribe((page1) -> {
            this.page = page1;
            loadPageFromApi(false);
        });
        this.repositoryPresenter = repositoryPresenter;
    }

    public void startLoad(boolean isRefreshing) {
        loadPageFromApi(isRefreshing);
    }

    public void loadPageFromApi(boolean isRefreshing) {
        if (!isRefreshing) {
            setCorrectLoading();
        } else {
          page = 1;
          items.clear();
        }
        githubManager.getGithubRepositories(this.page)
                .map(RequestGithub::getRepositories)
                .toObservable()
                .flatMapIterable(repos -> repos)
                .map(repos -> new RepositoryRowViewModel(this,repos))
                .toList()
                .subscribe(repos -> {
                    items.add(new PaginatorViewModel(page + "º"));
                    items.addAll(repos);
                    disableProgress();
                }, throwable -> {
                    repositoryPresenter.showError(throwable);
                    disableProgress();
                });
    }

    void click(Repository repository) {
        repositoryPresenter.showPullRequestActivity(repository.getOwner().getName(), repository.getName());
    }

    private void setCorrectLoading() {
        if(items.size() > 0) {
            progressBarVisibility.set(true);
            progressBarInitialLoadingVisibility.set(false);
        } else {
            progressBarInitialLoadingVisibility.set(true);
            progressBarVisibility.set(false);
        }
    }

    private void disableProgress() {
        progressBarVisibility.set(false);
        progressBarInitialLoadingVisibility.set(false);
        repositoryPresenter.stopRefresh();
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public ObservableField<Boolean> getProgressBarVisibility() {
        return progressBarVisibility;
    }

    public ObservableArrayList<CommonPaginatorInterfaceRow> getItems() {
        return items;
    }

    public final ItemViewSelector<CommonPaginatorInterfaceRow> itemView = new BaseItemViewSelector<CommonPaginatorInterfaceRow>() {
        @Override
        public void select(ItemView itemView, int position, CommonPaginatorInterfaceRow item) {
            if(item instanceof RepositoryRowViewModel) {
                itemView.set(BR.viewModel, R.layout.repository_row);
            } else {
                itemView.set(BR.viewModel, R.layout.paginator_row);
            }
        }
    };
}
