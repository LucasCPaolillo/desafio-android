package com.desafio.desafio_android.viewmodel;

import com.desafio.desafio_android.model.PullRequest;

public class PullRequestRowViewModel {
    PullRequest pullRequest;
    PullRequestViewModel pullRequestViewModel;

    public PullRequestRowViewModel(PullRequestViewModel pullRequestViewModel, PullRequest pullRequest) {
        this.pullRequest = pullRequest;
        this.pullRequestViewModel = pullRequestViewModel;
    }

    public void click() {
        pullRequestViewModel.click(pullRequest.getUrl());
    }

    public PullRequest getPullRequest() {
        return pullRequest;
    }
}
