package com.desafio.desafio_android.viewmodel;

import android.databinding.ObservableField;

import com.desafio.desafio_android.manager.GithubManager;

public class CommonViewModel {

    ObservableField<Boolean> progressBarInitialLoadingVisibility;
    GithubManager githubManager;

    public CommonViewModel(GithubManager githubManager) {
        this.progressBarInitialLoadingVisibility = new ObservableField<>(false);
        this.githubManager = githubManager;
    }

    public ObservableField<Boolean> getProgressBarInitialLoadingVisibility() {
        return progressBarInitialLoadingVisibility;
    }

}
