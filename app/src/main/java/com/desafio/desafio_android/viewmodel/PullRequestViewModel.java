package com.desafio.desafio_android.viewmodel;

import android.databinding.ObservableArrayList;

import com.desafio.desafio_android.BR;
import com.desafio.desafio_android.R;
import com.desafio.desafio_android.manager.GithubManager;
import com.desafio.desafio_android.presenter.PullRequestPresenter;

import java.io.Serializable;

import me.tatarka.bindingcollectionadapter.BaseItemViewSelector;
import me.tatarka.bindingcollectionadapter.ItemView;
import me.tatarka.bindingcollectionadapter.ItemViewSelector;

public class PullRequestViewModel extends CommonViewModel implements Serializable {

    private ObservableArrayList<PullRequestRowViewModel> items;
    private String creator;
    private String repository;
    private PullRequestPresenter pullRequestPresenter;

    public PullRequestViewModel(GithubManager githubManager, String creator, String repository, PullRequestPresenter pullRequestPresenter) {
        super(githubManager);
        this.pullRequestPresenter = pullRequestPresenter;
        this.items = new ObservableArrayList<>();
        this.creator = creator;
        this.repository = repository;
    }

    public void loadPullRequestsFromApi(boolean isRefresing) {
            progressBarInitialLoadingVisibility.set(!isRefresing);
            githubManager.getPullRequests(creator, repository)
                    .toObservable()
                    .flatMapIterable(pullRequests -> pullRequests)
                    .map(pullRequest -> new PullRequestRowViewModel(this, pullRequest))
                    .toList()
                    .subscribe(pullRequests -> {
                        if(pullRequests.size() == 0) {
                            pullRequestPresenter.showEmptyError();
                        }
                        items.addAll(pullRequests);
                        disableLoading();
                    }, throwable -> {
                        disableLoading();
                    });
    }

    private void disableLoading() {
        progressBarInitialLoadingVisibility.set(false);
        pullRequestPresenter.stopRefresh();
    }

    public ObservableArrayList<PullRequestRowViewModel> getItems() {
        return items;
    }

    public final ItemViewSelector<PullRequestRowViewModel> itemView = new BaseItemViewSelector<PullRequestRowViewModel>() {
        @Override
        public void select(ItemView itemView, int position, PullRequestRowViewModel item) {
            itemView.set(BR.viewModel, R.layout.pullrequest_row);
        }
    };

    void click(String url) {
        pullRequestPresenter.showWebView(url);
    }
}
