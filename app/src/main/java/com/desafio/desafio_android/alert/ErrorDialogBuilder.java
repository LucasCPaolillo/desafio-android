package com.desafio.desafio_android.alert;

import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.StringRes;
import android.support.v7.app.AlertDialog;
import android.view.WindowManager;

import com.desafio.desafio_android.R;


public class ErrorDialogBuilder {

    private final Throwable exception;
    private final String msg;
    private final Context context;
    private AlertDialog.Builder dialogBuilder;

    public ErrorDialogBuilder(Throwable exception, Context context) {
        this.exception = exception;
        this.context = context;
        this.createDialogBuilder();
        this.msg = "";
    }

    public ErrorDialogBuilder(String msg, Context context) {
        this.msg = msg;
        this.context = context;
        this.createDialogBuilderWithMsg();
        this.exception = null;
    }

    private void createDialogBuilderWithMsg() {
        final String title = context.getString(R.string.error_alert_title);

        this.dialogBuilder = new AlertDialog.Builder(context)
                .setTitle(title)
                .setMessage(msg)
                .setCancelable(false)
                .setNegativeButton("OK", null);
    }

    @SuppressWarnings("unchecked")
    private void createDialogBuilder() {

        final String title = context.getString(R.string.error_alert_title);
        final String msg = ErrorMessageFactory.getError(exception, context);

        this.dialogBuilder = new AlertDialog.Builder(context)
                .setTitle(title)
                .setMessage(msg)
                .setCancelable(false)
                .setNegativeButton("OK", null);
    }

    public ErrorDialogBuilder setPositiveButton(@StringRes int textId,
                                                DialogInterface.OnClickListener listener) {
        dialogBuilder.setPositiveButton(context.getString(textId), listener);
        return this;
    }

    public ErrorDialogBuilder setCancelButton(DialogInterface.OnClickListener listener) {
        final String text = (context.getString(android.R.string.cancel));
        return this.setNegativeButton(text, listener);
    }

    private ErrorDialogBuilder setNegativeButton(String text,
                                                 DialogInterface.OnClickListener listener) {
        dialogBuilder.setNegativeButton(text, listener);
        return this;
    }

    public void show() {
        try {
            dialogBuilder.create().show();
        } catch (final IllegalArgumentException | WindowManager.BadTokenException ignored) {
            // java.lang.IllegalArgumentException: View not attached to window manager
            // has leaked window
        }
    }

}
