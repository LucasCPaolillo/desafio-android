package com.desafio.desafio_android.alert;

import android.content.Context;

import com.desafio.desafio_android.R;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jakewharton.retrofit2.adapter.rxjava2.HttpException;

import java.io.IOException;
import java.net.UnknownHostException;

import okhttp3.ResponseBody;

public class ErrorMessageFactory {

    private static String getErrorMessage(final ResponseBody body) {
        final ObjectMapper mapper = new ObjectMapper();
        try {
            JsonNode jsonResponse = mapper.readTree(body.byteStream());
            return jsonResponse.get("message").asText();
        } catch (IOException e) {
            return null;
        }
    }

    public static String getError(final Throwable exception, final Context context) {
        if (exception instanceof HttpException) {
            retrofit2.Response<?> response = ((HttpException) exception).response();

            if (response != null && response.errorBody() != null) {
                final String apiError = getErrorMessage(response.errorBody());
                if (apiError != null) {
                    return apiError;
                }
            }
        } else if (exception instanceof UnknownHostException) {
            return context.getString(R.string.fail_to_fetch_no_internet);
        }
        return context.getString(R.string.error_api);
    }
}
