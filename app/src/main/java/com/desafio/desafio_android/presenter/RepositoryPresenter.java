package com.desafio.desafio_android.presenter;

/**
 * Created by lucascferraz on 16/01/17.
 */

public interface RepositoryPresenter extends CommonPresenter {
    void showPullRequestActivity(String creator, String repository);
}
