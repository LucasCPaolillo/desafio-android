package com.desafio.desafio_android.presenter;

public interface PullRequestPresenter extends CommonPresenter {
    void showWebView(String url);
    void showEmptyError();
}
