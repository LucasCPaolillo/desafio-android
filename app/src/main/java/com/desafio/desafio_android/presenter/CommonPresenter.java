package com.desafio.desafio_android.presenter;

public interface CommonPresenter {
    void showError(Throwable error);
    void stopRefresh();
}
