package com.desafio.desafio_android.binding;

import android.databinding.BindingAdapter;
import android.view.View;

import com.bumptech.glide.Glide;
import com.mikhaellopez.circularimageview.CircularImageView;

public class CircularImageViewBindingAdapter {
    @BindingAdapter("android:background")
    public static void setBackground(CircularImageView view, String avatarUrl) {
        Glide.with(view.getContext()).load(avatarUrl)
                .asBitmap()
                .placeholder(android.support.v7.appcompat.R.drawable.abc_ab_share_pack_mtrl_alpha)
                .into(view);
    }
}
