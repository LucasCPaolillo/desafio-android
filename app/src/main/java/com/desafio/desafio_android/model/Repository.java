package com.desafio.desafio_android.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Repository {
    private String name;
    private String description;
    @JsonProperty("stargazers_count")
    private int startCount;
    @JsonProperty("forks")
    private int forkCount;
    private GithubUser owner;

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public GithubUser getOwner() {
        return owner;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setOwner(GithubUser owner) {
        this.owner = owner;
    }

    public String getStartCount() {
        return String.valueOf(startCount);
    }

    public String getForkCount() {
        return String.valueOf(forkCount);
    }

    public void setStartCount(int startCount) {
        this.startCount = startCount;
    }

    public void setForkCount(int forkCount) {
        this.forkCount = forkCount;
    }
}
