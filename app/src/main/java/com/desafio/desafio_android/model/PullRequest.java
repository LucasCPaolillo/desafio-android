package com.desafio.desafio_android.model;

import com.desafio.desafio_android.client.GithubClient;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PullRequest {
    private GithubUser user;
    private String title;
    @JsonProperty("created_at")
    private Date date;
    private String body;
    @JsonProperty("html_url")
    private String url;

    public GithubUser getUser() {
        return user;
    }

    public void setUser(GithubUser user) {
        this.user = user;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDate() {
        SimpleDateFormat dt = new SimpleDateFormat("dd/MM/yy");
        return dt.format(date);
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getUrl() {
        return url;
    }
}
