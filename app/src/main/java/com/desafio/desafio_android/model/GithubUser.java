package com.desafio.desafio_android.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class GithubUser {
    @JsonProperty("login")
    private String name;
    @JsonProperty("avatar_url")
    private String avatarUrl;


    public String getName() {
        return name;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }


    public void setName(String name) {
        this.name = name;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }


}
