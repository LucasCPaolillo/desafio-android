package com.desafio.desafio_android.manager;

import com.desafio.desafio_android.client.GithubClient;
import com.desafio.desafio_android.model.PullRequest;
import com.desafio.desafio_android.model.RequestGithub;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class GithubManagerImpl implements GithubManager{

    GithubClient githubClient;

    public GithubManagerImpl(GithubClient githubClient) {
        this.githubClient = githubClient;
    }

    @Override
    public Single<RequestGithub> getGithubRepositories(int page) {
        return githubClient.getRepositories("language:Java", "stars", page)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Single<List<PullRequest>> getPullRequests(String creator, String repository) {
        return githubClient.getPullRequets(creator, repository)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }


}
