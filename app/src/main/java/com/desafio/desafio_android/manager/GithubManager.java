package com.desafio.desafio_android.manager;

import com.desafio.desafio_android.model.PullRequest;
import com.desafio.desafio_android.model.RequestGithub;

import java.util.List;

import io.reactivex.Single;

public interface GithubManager {
    Single<RequestGithub> getGithubRepositories(int page);
    Single<List<PullRequest>> getPullRequests(String creator, String repository);
}
