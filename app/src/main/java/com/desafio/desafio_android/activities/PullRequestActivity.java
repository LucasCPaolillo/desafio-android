package com.desafio.desafio_android.activities;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;

import com.desafio.desafio_android.R;
import com.desafio.desafio_android.alert.ErrorDialogBuilder;
import com.desafio.desafio_android.application.GithubApplication;
import com.desafio.desafio_android.databinding.ActivityPullRequestBinding;
import com.desafio.desafio_android.manager.GithubManager;
import com.desafio.desafio_android.presenter.PullRequestPresenter;
import com.desafio.desafio_android.viewmodel.PullRequestViewModel;
import com.desafio.desafio_android.viewmodel.RepositoryViewModel;

import javax.inject.Inject;

public class PullRequestActivity extends AppCompatActivity implements PullRequestPresenter {

    PullRequestViewModel pullRequestViewModel;

    @Inject
    GithubManager githubManager;

    ActivityPullRequestBinding binding;
    private String creator;
    private String repository;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ((GithubApplication) getApplication())
                .getApplicationComponent()
                .inject(this);


        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        this.binding = DataBindingUtil.setContentView(this, R.layout.activity_pull_request);
        DividerItemDecoration mDividerItemDecoration = new DividerItemDecoration(binding.pullRequestsRecyclerView.getContext(),
                linearLayoutManager.getOrientation());
        this.binding.pullRequestsRecyclerView.addItemDecoration(mDividerItemDecoration);
        this.binding.pullRequestsRecyclerView.setLayoutManager(linearLayoutManager);


        creator = getIntent().getStringExtra("creator");
        repository = getIntent().getStringExtra("repository");
        this.pullRequestViewModel = new PullRequestViewModel(this.githubManager, creator, repository, this);


        this.binding.setViewModel(pullRequestViewModel);

        this.setupToolbar();
        this.pullRequestViewModel.loadPullRequestsFromApi(false);
        this.swipeToRefreshSetup();
    }

    public void swipeToRefreshSetup() {
        this.binding.swipeToRefresh.setOnRefreshListener(() -> {
            this.pullRequestViewModel.loadPullRequestsFromApi(true);
        });
    }

    public void setupToolbar() {
        setSupportActionBar(this.binding.toolbar);
        if (this.getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            this.binding.toolbar.setNavigationOnClickListener(v -> onBackPressed());
            this.getSupportActionBar().setTitle(R.string.pr);
        }
        this.binding.toolbar.setTitleTextColor(ContextCompat.getColor(this, android.R.color.white));
    }


    @Override
    public void showWebView(String url) {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        this.startActivity(browserIntent);
    }

    @Override
    public void showEmptyError() {
        new ErrorDialogBuilder(getString(R.string.no_pr_here), this)
                .setCancelButton((dialog, which) -> finish())
                .setPositiveButton(R.string.try_again, (dialog, which) -> {
                    this.pullRequestViewModel.loadPullRequestsFromApi(false);
                })
                .show();
    }

    @Override
    public void showError(Throwable error) {
        new ErrorDialogBuilder(error, this)
                .setCancelButton((dialog, which) -> finish())
                .setPositiveButton(R.string.try_again, (dialog, which) -> {
                    this.pullRequestViewModel.loadPullRequestsFromApi(false);
                })
                .show();
    }

    @Override
    public void stopRefresh() {
        this.binding.swipeToRefresh.setRefreshing(false);
    }
}
