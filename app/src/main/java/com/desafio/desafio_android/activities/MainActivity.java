package com.desafio.desafio_android.activities;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;

import com.desafio.desafio_android.R;
import com.desafio.desafio_android.alert.ErrorDialogBuilder;
import com.desafio.desafio_android.application.GithubApplication;
import com.desafio.desafio_android.databinding.ActivityMainBinding;
import com.desafio.desafio_android.listener.EndlessScrollListener;
import com.desafio.desafio_android.manager.GithubManager;
import com.desafio.desafio_android.presenter.RepositoryPresenter;
import com.desafio.desafio_android.viewmodel.RepositoryViewModel;

import javax.inject.Inject;

public class MainActivity extends AppCompatActivity implements RepositoryPresenter {

    RepositoryViewModel repositoryViewModel;
    private EndlessScrollListener scrollListener;

    @Inject
    GithubManager githubManager;

    private ActivityMainBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ((GithubApplication) getApplication())
                .getApplicationComponent()
                .inject(this);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);

        this.scrollListener = new EndlessScrollListener(linearLayoutManager);

        this.binding = DataBindingUtil.setContentView(this, R.layout.activity_main);

        DividerItemDecoration mDividerItemDecoration = new DividerItemDecoration(binding.repositoriesRecyclerView.getContext(),
                linearLayoutManager.getOrientation());

        this.binding.repositoriesRecyclerView.addOnScrollListener(scrollListener);
        this.binding.repositoriesRecyclerView.addItemDecoration(mDividerItemDecoration);
        this.binding.repositoriesRecyclerView.setLayoutManager(linearLayoutManager);

        this.repositoryViewModel = new RepositoryViewModel(scrollListener.getCurrentPageObservable(), githubManager, this);

        this.binding.setViewModel(repositoryViewModel);

        this.setupToolbar();

        this.repositoryViewModel.startLoad(false);
        this.swipeToRefreshSetup();
    }

    public void setupToolbar() {
        setSupportActionBar(this.binding.toolbar);
        if (this.getSupportActionBar() != null) {
            this.getSupportActionBar().setTitle("Repositórios");
        }
        this.binding.toolbar.setTitleTextColor(ContextCompat.getColor(this, android.R.color.white));
    }

    public void swipeToRefreshSetup() {
        this.binding.swipeToRefresh.setOnRefreshListener(() -> {
            this.scrollListener.restart();
            this.repositoryViewModel.setPage(1);
            this.repositoryViewModel.startLoad(true);
        });
    }

    @Override
    public void showPullRequestActivity(String creator, String repository) {
        Intent i = new Intent(this, PullRequestActivity.class);
        i.putExtra("creator", creator);
        i.putExtra("repository", repository);
        startActivity(i);
    }

    @Override
    public void showError(Throwable error) {
        new ErrorDialogBuilder(error,this)
                .setCancelButton((dialog, which) -> finish())
                .setPositiveButton(R.string.try_again, (dialog, which) -> {
                    repositoryViewModel.loadPageFromApi(false);
                })
                .show();
    }

    @Override
    public void stopRefresh() {
        this.binding.swipeToRefresh.setRefreshing(false);
    }

}
