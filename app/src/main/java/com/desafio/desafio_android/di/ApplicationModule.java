package com.desafio.desafio_android.di;


import com.desafio.desafio_android.client.GithubClient;
import com.desafio.desafio_android.manager.GithubManager;
import com.desafio.desafio_android.manager.GithubManagerImpl;
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

@Module
public class ApplicationModule {
    private String apiUrl;

    public ApplicationModule(String apiUrl) {
        this.apiUrl = apiUrl;
    }


    @Provides
    @Singleton
    GithubManager providesGithubManager(GithubClient client) {
        return new GithubManagerImpl(client);
    }

    @Provides
    @Singleton
    GithubClient provideClient() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(apiUrl)
                .client(client)
                .addConverterFactory(JacksonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();

        return retrofit.create(GithubClient.class);
    }
}
