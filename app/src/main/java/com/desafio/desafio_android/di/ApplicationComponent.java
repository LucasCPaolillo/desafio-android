package com.desafio.desafio_android.di;

import com.desafio.desafio_android.activities.MainActivity;
import com.desafio.desafio_android.activities.PullRequestActivity;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {ApplicationModule.class})
public interface ApplicationComponent {
    void inject(MainActivity mainActivity);
    void inject(PullRequestActivity pullRequestActivity);
}
